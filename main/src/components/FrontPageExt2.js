import { useState, useEffect } from 'react';
import axios from "axios";
import moment from "moment";


const FrontPageExt2 = () => {

    const [keikka, setKeikka] = useState([]);


    useEffect(() => {
        axios.get('https://anyverse-backend.onrender.com/haekeikat')
            .then(response => {
                const sortFetch = response.data.sort((a, b) => { //Uusin keikka ekana
                    return new Date(b.when) - new Date(a.when);
                });
                setKeikka(sortFetch);

            })
            .catch(error => {
                console.log(error);
            });
    }, []);

    return (
        <div id='frontpageeext2'>


            <div className='gigH1s'>
                <div className='gigH1box'>
                    <div className='mitämissäkoskaparagraph'>
                        <h1>TOURING</h1>
                        <hr />


                    </div>


                    {keikka.map(item => (
                        <div id='touringDetails' key={item.id}>
                            <div className='touringDetailsLeftSide'>
                                <p>{moment(item.when).format('DD.MM.YYYY')}</p>
                                <br />
                                <p id='touringDetailsPlace'>{item.what}</p>
                                <p id='touringDetailsPlace'>{item.where} <b>klo {item.clock}</b></p>

                            </div>
                            <br />
                            <div className='touringDetailsRightSide'>
                                <p key={item._id}>Liput: {item.cost} €</p>
                                {item.link ? (
                                    <p id='lisatietoaLink' onClick={() => window.open(`${item.link}`, "_blank")}>Lisätietoa</p>
                                ) : null}
                            </div>
                        </div>
                    ))}




                </div>


                {/*  <h1 id="tulevatkeikath1">TÄNNE SEURAAVAKSI</h1> */}



            </div>


            {  /* <div className='gigH1IMAGE'>
                <div className='gigH1ImageDiv'>
                    <h1>Corolla op</h1>
                </div>
            </div> */ }
        </div>
    )
}

export default FrontPageExt2