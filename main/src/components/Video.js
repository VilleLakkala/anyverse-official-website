import React from 'react'
import videoBg from "../assets/video.mp4"

const Video = () => {
  return (
    <div className='video'>
        <div className='overlay'></div>
        <video src={videoBg} autoPlay loop />
        <div className='welcome'>
           

        </div>
    </div>
  )
}

export default Video