import React from 'react'

import "animate.css"
import { useState, useRef } from 'react'



const FrontPage = () => {

  //const [admin, setAdmin] = useState(false)

  let bg = useRef();

  function ref() {
    bg.current.id = "backgroundSWITCH"  // Switch the background, this causes the illusion of glitch
  }
  function ref1() {
    bg.current.id = "background"
  }

  {/* 
  const interval = setInterval(ref, 2000)
  const interval2 = setInterval(ref1, 2100)   //The screen tearing glitch effect timings
  const interval3 = setInterval(ref, 2500)
  const interval4 = setInterval(ref1, 2600)
  */}

  return (
    <>


      <div>

        <div id='background' ref={bg}>

          <h1 id='h1'>"...Anyverse is an energetic rock band from Sodankylä, Finland, focused on making real, loud and hooky rock music great again. The band started as a DIY studio project in 2018 and has since grown to practicing for shows and recording a full-length album due to be released in 2023. With sing-along choruses, growling riffs, and emotional lyrics, Anyverse has all the makings of a great rock band. Their creativity extends to imagery and videography as seen in their self-made covers and music videos. Get ready to rock with Anyverse...". <br /> <br /><span id='sodehCredits'> -Sodeh Records</span></h1>

          <h1 id='h1_2'> <br /> <br /><span id='sodehCredits'> </span></h1>

        </div>

        <h1 id='meetTheCrew'>MEET THE CREW</h1>

        <div id='bandMembersFrame'>



          <div className='bandMemberCard aleksi'>

            <div className='testi666'>
              <div id='aleksiHalminen'></div>
              <div className='centerDivMembers'>

                <h1>Aleksi Halminen</h1>
                <p>Guitar & Vocals</p>

              </div>
            </div>
          </div>

          <div className='bandMemberCard rami'>
            <div className='testi666'>
              <div id='ramiTammela'></div>
              <div className='centerDivMembers'>
                <h1>Rami Tammela</h1>
                <p>Drums</p>
              </div>
            </div>
          </div>
          <div className='bandMemberCard sami'>

            <div className='testi666'>
              <div id='samiAlatalo'></div>
              <div className='centerDivMembers'>
                <h1>Sami Alatalo</h1>
                <p>Bass</p>
              </div>
            </div>
          </div>



        </div>



      </div>
      <div id='spotifyPlayer'>


        <iframe src="https://open.spotify.com/embed/artist/5n7wwlclR6kAlw5N74NCSg?utm_source=generator&theme=0" position="relative" left="0" width="40%" height="300px" frameBorder="0" allowFullScreen="" allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture" loading="lazy" id="SPOTIFYiframe"></iframe>


      </div>

    </>
  )
}

export default FrontPage 