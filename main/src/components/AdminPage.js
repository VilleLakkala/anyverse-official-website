import React from 'react'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import logo from "../assets/bestlogo.png"
import "animate.css"
import axios from "axios";




const AdminPage = ({ logOff }) => {

  const navigate = useNavigate();

  const [what, setWhat] = useState(null)
  const [where, setWhere] = useState(null)
  const [clock, setClock] = useState(null)
  const [when, setWhen] = useState(null)
  const [cost, setCost] = useState(null)
  const [link, setLink] = useState(null)
  const [keikka, setKeikka] = useState(null)




  const [toggleGig, setToggleGig] = useState(false);
  const [blog, setBlog] = useState(false);



  const token = localStorage.getItem("token") //JWT 

  let dataGig = { //Post requestia varten
    what: what,
    where: where,
    clock: clock,
    when: when,
    cost: cost,
    link: link
  }

  function logOffLaunch() {
    logOff();
  }

  async function checkJWT() { // Kun tullaan admin sivulle, tarkistetaan JWT  
    await fetch("https://anyverse-backend.onrender.com/tarkistajwt", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",

      },
    }).then(response => response.json())
      .then(response => {
        localStorage.setItem("isVerified", true)
        const authenticated = localStorage.getItem('isVerified');
        //console.log("Verified?", authenticated) // Varmistetaan, että JWT token on tarkistettu oikein
      })
      .catch(error => {
        console.log(error);
      });
  }


  useEffect(() => { //JWT check 
    checkJWT();
  }, [])


  useEffect(() => {   //Hae keikat näkyviin
    axios.get('https://anyverse-backend.onrender.com/haekeikat')
      .then(response => {
        const sortFetch = response.data.sort((a, b) => { //Uusin keikka ekana
          return new Date(b.when) - new Date(a.when);
        });
        setKeikka(sortFetch);

      })
      .catch(error => {
        console.log(error);
      });
  }, []);

  async function haeKeikat() {
    axios.get('https://anyverse-backend.onrender.com/haekeikat')
      .then(response => {
        const sortFetch = response.data.sort((a, b) => { //Uusin keikka ekana
          return new Date(b.when) - new Date(a.when);
        });
        setKeikka(sortFetch);

      })
      .catch(error => {
        console.log(error);
      });
  }

  async function newPost() {
    await fetch("https://anyverse-backend.onrender.com/luotapahtuma", {
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",

      },
      body: JSON.stringify(dataGig)
    }).then(response => response.json())
      .then(response => {
        console.log(response)
        setWhat("")
        setWhen("")
        setWhere("")
        setCost("")
        setClock("")
        setLink("")
      })
      .catch(error => {
        console.log(error);
      });
    haeKeikat();
  }

  /*
   async function newUpdate() { //scrapped feature for now  
     await fetch("http://localhost:3001/luopaivitys", {
       method: "POST",
       headers: {
         Authorization: `Bearer ${token}`,
         "Content-Type": "application/json",
 
       },
       body: JSON.stringify(dataPost)
     }).then(response => response.json())
       .then(response => {
         console.log(response)
         setTitle("")
         setContent("")
 
       })
       .catch(error => {
         console.log(error);
         setTitle("")
         setContent("")
       });
   }
 
 */


  const headers = {
    headers: { Authorization: `Bearer ${token}` }
  };

  async function poistaKeikka(id) {
    await axios.delete(`https://anyverse-backend.onrender.com/poistakeikka/${id}`, headers);
    await haeKeikat();
  }


  return (
    <>
      <div className='adminLegacy'>

        <nav className='adminNavBar'>
          <div className='adminPanelLogo'>
            <img src={logo} id="logo1" />

            <button onClick={logOffLaunch}>KIRJAUDU ULOS</button>
            <button onClick={() => {
              setBlog(!blog);
            }
            }>Aktiiviset keikat</button>

            <br />
        
          </div>
        </nav>


        <h1 id='adminSivuH1'>ADMIN SIVU</h1>




        <div className='animate__animated animate__fadeIn newGigForm'>
          <h1>UUSI KEIKKA</h1>
          <input type="text" placeholder='Mitä' value={what}
            onChange={(e) => setWhat(e.target.value)}
          />
          <input type="text" placeholder='Missä' value={where}
            onChange={(e) => setWhere(e.target.value)}
          />
          <input type="date" placeholder='Koska' value={when}
            onChange={(e) => setWhen(e.target.value)}
          />
          <input type="text" placeholder='Kellon aika (esim. 17.00)' value={clock}
            onChange={(e) => setClock(e.target.value)}
          />
          <input type="number" placeholder='Lipun hinta €' value={cost}
            onChange={(e) => setCost(e.target.value)}
          />
          <input type="text" placeholder='Linkki tapahtumaan, esim lippuihin/tarkempaa tietoa (ei pakollinen)' value={link}
            onChange={(e) => setLink(e.target.value)}
            id="linkki" />
          <button onClick={newPost}>LISÄÄ KEIKKA</button>
        </div>


        {blog &&

          <div className='animate__animated animate__fadeIn poistaKeikka'>
            <h1>Kaikki aktiiviset keikat näkyvät alhaalla</h1>
            {keikka.map(item => (
              <div className='poistaKeikkaButtonDiv' key={item._id}>
                <p>{item.what}</p>
                <button onClick={() => { poistaKeikka(item._id) }}>POISTA</button>

              </div>

            ))}
          </div>

        }



      </div>
    </>
  )
}

export default AdminPage