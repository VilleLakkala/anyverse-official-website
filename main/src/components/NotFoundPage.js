import React from 'react'

const NotFoundPage = () => {
    return (
        <div>
            <h1 id='notFound'> 404 Error</h1>
        </div>
    )
}

export default NotFoundPage