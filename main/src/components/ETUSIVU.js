import React from 'react'
import Navbar from './Navbar';
import FrontPage from './FrontPage';
import FrontPageExt1 from './FrontPageExt1';
import FrontPageExt2 from "./FrontPageExt2"

const ETUSIVU = () => {
  return (
    <>
    
    <div className='app'>
          <FrontPageExt1 />
    </div>
    <FrontPageExt2 />
    <FrontPage />
    
    </>
  )
}

export default ETUSIVU