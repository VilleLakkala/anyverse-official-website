import React from 'react'
import axios from 'axios';
import { useState } from 'react';
import "animate.css"
import logo from "../assets/bestlogo.png"
import fb from "../assets/fb.png"
import ig from "../assets/ig.png"
import spotify from "../assets/spotify.png"
import yt from "../assets/yt.png"
import sodeh from "../assets/sodeh.png"
import adminIcon from "../assets/adminicon.png"
import { useNavigate } from 'react-router-dom';





const Navbar = ({ setAuthenticated }) => {

  const navigate = useNavigate();

  const [admin, setAdmin] = useState(false)
  const [username, setUsername] = useState()
  const [password, setPassword] = useState()

  const [menu, setMenu] = useState(false)

  function clearInput() {
    setPassword("")
    setUsername("")

  }

  function redirectToAdminPage(response) {
    if (response.data.user) {
      //console.log(response.data.token)
      localStorage.setItem("token", response.data.token);
      setAuthenticated(true)
      setTimeout(() => {
        navigate('/admin');
      }, 600);

    }
  }

  function scrollToHome() { //Scroll into view funktiot
    const element = document.getElementById("contentmain");
    element.scrollIntoView();
  }

  function scrollToHomeMobile() { //MOBILE
    const element = document.getElementById("contentmain");
    setMenu(!menu)
    element.scrollIntoView();
  }

  function scrollToKeikat() {
    const element = document.getElementById("frontpageeext2")
    element.scrollIntoView();
  }

  function scrollToKeikatMobile() { //MOBILE
    const element = document.getElementById("frontpageeext2")
    element.scrollIntoView();
    setMenu(!menu)
  }


  function scrollToInfo() {
    const element = document.getElementById("background")
    element.scrollIntoView();
  }
  function scrollToInfoMobile() { //MOBILE
    const element = document.getElementById("h1")
    element.scrollIntoView();
    setMenu(!menu)
  }
  function scrollToCrew() {
    const element = document.getElementById("bandMembersFrame")
    element.scrollIntoView();
  }
  function scrollToCrewMobile() {
    const element = document.getElementById("bandMembersFrame")
    element.scrollIntoView();
  }

  const data = { //Payload kirjautumiselle
    username: username,
    password: password
  };

  async function logIn() {
    await axios.post("https://anyverse-backend.onrender.com/login", data)
      .then(response => {
        console.log(response.data);
        clearInput();//tyhjennä input fieldit
        redirectToAdminPage(response);  //Ohjataan admin sivulle jos onnistunut sis.kirjautuminen. Otetaan response(kirj.tiedot) parametriksi
      })
      .catch(error => {
        console.log(error);
        clearInput();
      });
  }


  return (


    <>

      <div id='dropdownMenu' onClick={() => { setMenu(!menu) }}> </div>

      {menu &&
        <div className='animate__animated animate__slideInLeft dropdownMenu_valikko'>
          <img src={logo} alt="anyverseLogo" id='anyverseDropDownMenu' />
          <h1 onClick={scrollToHomeMobile}>ETUSIVU</h1>
          <h1 onClick={scrollToKeikatMobile}>KEIKAT</h1>
          <h1 onClick={scrollToInfoMobile}>ANYVERSE</h1>
          <h1 onClick={scrollToCrewMobile}>THE CREW</h1>
          <div id='dropdownSocialIcons'>
            <img src={fb} className="SOCIALMEDIA2"  alt="facebook" onClick={() => window.open("https://www.facebook.com/anyverseofficial", "_blank")} />
            <img src={ig} className="SOCIALMEDIA2"  alt="instagram" onClick={() => window.open("https://www.instagram.com/anyversreofficial/", "_blank")} />
            <img src={spotify} className="SOCIALMEDIA2"  alt="spotify" onClick={() => window.open("https://open.spotify.com/artist/5n7wwlclR6kAlw5N74NCSg?si=KezH1nMeSdiimYEHuilr-Q", "_blank")} />
            <img src={yt} className="SOCIALMEDIA2"  alt="youtube" onClick={() => window.open("https://www.youtube.com/@anyverse", "_blank")} />
            <img src={sodeh} className="SOCIALMEDIA2" alt="youtube" onClick={() => window.open("https://www.sodeh.ca/", "_blank")} />
            <img src={adminIcon} className="SOCIALMEDIA2"  alt="Admin" onClick={() => {
              setAdmin(!admin)
            }} />
          </div>
        </div>
      }
      <nav className='navbar'>
        <div>
          <img src={logo} id="logo" alt="logo"

          />
        </div>

        <div className='navbarLinks'>
          <p onClick={scrollToHome}>Etusivu</p>
          <p onClick={scrollToKeikat}>Keikat</p>
          <p onClick={scrollToInfo}>Anyverse</p>
          <p onClick={scrollToCrew}>THE CREW</p>
        </div>


        <div className='socialMedia'>
          <img src={fb} className="SOCIALMEDIA" id='facebook' alt="facebook" onClick={() => window.open("https://www.facebook.com/anyverseofficial", "_blank")} />
          <img src={ig} className="SOCIALMEDIA" id='instagram' alt="instagram" onClick={() => window.open("https://www.instagram.com/anyversreofficial/", "_blank")} />
          <img src={spotify} className="SOCIALMEDIA" id='spotify' alt="spotify" onClick={() => window.open("https://open.spotify.com/artist/5n7wwlclR6kAlw5N74NCSg?si=KezH1nMeSdiimYEHuilr-Q", "_blank")} />
          <img src={yt} className="SOCIALMEDIA" id='yt' alt="youtube" onClick={() => window.open("https://www.youtube.com/@anyverse", "_blank")} />
          <img src={sodeh} className="SOCIALMEDIA" id='sodeh' alt="youtube" onClick={() => window.open("https://www.sodeh.ca/", "_blank")} />
          <img src={adminIcon} className="SOCIALMEDIA" id='adminIcon' alt="Admin" onClick={() => {
            setAdmin(!admin)
          }} />
        </div>

      </nav>

      {admin &&
        <div className='animate__animated animate__fadeInLeft inputfields'>
          <input type="text" placeholder='admin tunnus' value={username}
            onChange={(e) => setUsername(e.target.value)} />

          <input type="password" placeholder='admin salasana' value={password}
            onChange={(e) => setPassword(e.target.value)} />

          <button type='button' onClick={logIn}>Kirjaudu sisään</button>
        </div>
      }




    </>

    /*
    <div className='navbarLinks'>
      <p onClick={scrollToGigs}>Gigs</p>
      <p onClick={scrollToHome}>Home</p>
      <p>Something</p>
    </div>
    */









  )
}

export default Navbar