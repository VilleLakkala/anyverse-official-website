
import './App.css';
import "animate.css";
import { Route, Routes } from "react-router-dom";

import ETUSIVU from './components/ETUSIVU';
import Navbar from './components/Navbar';
import AdminPage from './components/AdminPage';
import NotFoundPage from './components/NotFoundPage';
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';



function App() {

  const navigate = useNavigate();
  const token = localStorage.getItem('token');
  const [authenticated, setAuthenticated] = useState(localStorage.getItem('isVerified'));

  function logOff() {
    localStorage.clear();
    setAuthenticated(false);
    navigate('/');
  }

  useEffect(() => {
    if (!authenticated) {
      navigate('/', { replace: true });
    }
  }, [authenticated, navigate]);

  return (
    <>

      

     {!authenticated && <Navbar setAuthenticated={setAuthenticated} /> }
      {/* Etusivu sis. 3 komponenttia: navbar, FrontPage, FrontPageExt1 */}
      <Routes>

        <Route path='/' element={<ETUSIVU  />} />

        {token &&
          <Route path='/admin' element={<AdminPage logOff={logOff} />} />
        }

        <Route path="*" element={<NotFoundPage />} />

      </Routes>

    </>
  );
}

export default App;
