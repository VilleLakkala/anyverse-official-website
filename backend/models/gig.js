const mongoose = require("mongoose")

const gigSchema = new mongoose.Schema({
     what: {
        type:String,
        required: true
    },
     where: {
        type:String,
        required: true
    },
     clock: {
        type:String,
        required: true
    },
     when: {
        type: Date,
        required: true
    },
     cost: {
        type: Number,
        required: true
    },
     link: {
        type: String,
        
    }

})

module.exports = mongoose.model("gigs", gigSchema);