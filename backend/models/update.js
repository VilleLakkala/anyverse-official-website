const mongoose = require("mongoose")

const updateSchema = new mongoose.Schema({
     title: {
        type:String,
        required: true
    },
     content: {
        type:String,
        required: true
    }
})

module.exports = mongoose.model("updates", updateSchema);