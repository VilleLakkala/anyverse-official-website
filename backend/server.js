require("dotenv").config()
const express = require("express");
const app = express();
const mongoose = require("mongoose")
const cors = require("cors")

const User = require("./models/user")
const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")

const Gig = require("./models/gig")
const Update = require("./models/update")


//Sisältää kaikki routet


mongoose.connect(process.env.MONGO_DB, { useNewUrlParser: true })
const db = mongoose.connection;


app.listen(3001, () => {
    console.log("--Server started at port 3001--")
})

db.on("error", (err => console.log(err)))
db.once("open", () => console.log("--MongoDB on yhdistetty--"))

app.use(express.json());
app.use(cors());




//Routing. Since there is basically only admin log in and post, there is no need to setup folders
app.get("/", (req, res) => {
    res.send("Toimii heippa hei")       //server checkup
})

app.get("/haekeikat", async (req, res) => {
    try {
        const getGigs = await Gig.find().sort({ _id: -1 });; //Haetaan keikat mongodeebeestä, lajitellaan uusimman mukaan
        res.json(getGigs) //Palauttaa keikat jsoninä
    } catch (err) {
        res.json({ errorMsg: err })
    }
})

app.post("/register", async (req, res) => {   //register admin   Status: done

    const beecrypt = await bcrypt.hash(req.body.password, 10);

    const user = new User({
        username: req.body.username,
        password: beecrypt
    })

    try {
        const admin = await user.save();
        res.status(201).json(admin)
    } catch (error) {
        res.json({ error: error })
    }
})

app.post("/login", async (req, res) => {       //admin log in

    const user = await User.findOne({ username: req.body.username });

    if (user == null) {
        return console.log("no user found!");
    }

    try {
        if (await bcrypt.compare(req.body.password, user.password)) { //tarkista salasana, sen jälkeen jwt sign tulille
            const token = jwt.sign({ username: req.body.username }, process.env.JWT_SECRET, { expiresIn: "20min" })
            res.json({ token: token, user: true, username: "admin" })
            console.log("Login successful!")
        } else {
            console.log("JWT sign failed or wrong credentials")
        }
    } catch (error) {
        return res.status(500);
    }
})

app.post("/luotapahtuma", async (req, res) => {
    const gig = new Gig({
        what: req.body.what,
        where: req.body.where,
        clock: req.body.clock,
        when: req.body.when,
        cost: req.body.cost,
        link: req.body.link
    })

    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) return res.status(401).send('Unauthorized, no token'); //JWT check, tän olisi voinu kirjoittaa myös middlewareksi
        const decoded = jwt.verify(token, process.env.JWT_SECRET);


        const saveGig = await gig.save()
        res.json({ message: saveGig })

    } catch (error) {
        res.status(504).send('Internal Server Error!');
    }
})

app.post("/tarkistajwt", (req, res) => {
    try {
        const token = req.headers.authorization.split(' ')[1]; //Poimitaan frontin post pyynnön headers
        if (!token) return res.status(401).send('Unauthorized, no token'); //Jos tokenia ei ole ollenkaan

        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => { //jsonwebtoken oma funktio jwt.verify(); tarkistaa

            if (err) return res.status(401).send('Unauthorized!!! RIVI 88');     //Jos virhe

            res.status(200).json({ isVerified: true });
        });
    } catch (error) {
        res.status(500).send('Internal Server Error');
    }
})


app.post("/luopaivitys", async (req, res) => {
    const newUpdate = new Update({
        title: req.body.title,
        content: req.body.content
    })
    try {
        const token = req.headers.authorization.split(' ')[1];
        if (!token) return res.status(401).send('Unauthorized, no token');
        jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
            if (err) return res.status(401).send('Unauthorized!!');
        });

        const saveUpdate = await newUpdate.save()
        res.json({ message: saveUpdate })

    } catch (error) {
        res.status(504).send('Internal Server Error!');
    }
})

app.delete("/poistakeikka/:id", async (req, res) => {
    const id = req.params.id;

    try {

        const deletedGig = await Gig.findByIdAndDelete(id);

        if (!deletedGig) {
            return res.status(404).send({ error: 'Gig not found' });
        }

        res.status(204).send();
    } catch (error) {
        //error with the database query
        res.status(500).send({ error: 'Internal server error' });
    }

})